#!/bin/bash

direnv allow .
eval "$(direnv export bash)"
sphinx-quickstart -p "{{cookiecutter.project}}" -a "{{cookiecutter.author}}" \
      -v "0.1.0" --sep --ext-autodoc --no-batchfile -q docs > /dev/null
git init
git apply  << 'EOF'
From 7d6a7a4369146f06ff9ddbc91f23b2ead49e6888 Mon Sep 17 00:00:00 2001
From: Muhammad Alkarouri <malkarouri@tabahuth.com>
Date: Sun, 2 Apr 2017 18:48:00 +0100
Subject: [PATCH] Make documentation work

---
 docs/source/conf.py   | 6 +++---
 docs/source/index.rst | 1 +
 2 files changed, 4 insertions(+), 3 deletions(-)

diff --git a/docs/source/conf.py b/docs/source/conf.py
index 025cb43..b3f2aeb 100644
--- a/docs/source/conf.py
+++ b/docs/source/conf.py
@@ -16,9 +16,9 @@
 # add these directories to sys.path here. If the directory is relative to the
 # documentation root, use os.path.abspath to make it absolute, like shown here.
 #
-# import os
-# import sys
-# sys.path.insert(0, os.path.abspath('.'))
+import os
+import sys
+sys.path.insert(0, os.path.abspath('..'))


 # -- General configuration ------------------------------------------------
diff --git a/docs/source/index.rst b/docs/source/index.rst
index 1fcfc08..7b56566 100644
--- a/docs/source/index.rst
+++ b/docs/source/index.rst
@@ -10,6 +10,7 @@ Welcome to New Project's documentation!
    :maxdepth: 2
    :caption: Contents:

+   modules


 Indices and tables
--
2.9.2

EOF

sphinx-apidoc -o docs/source "{{cookiecutter.pkg_name}}"

git add .
