#!/usr/bin/env python
import re
import sys


MODULE_REGEX = r'^[_a-z][_a-z0-9]+$'

module_name = '{{ cookiecutter.pkg_name }}'

if not re.match(MODULE_REGEX, module_name):
    print('ERROR: %s is not an acceptable Python module name!' % module_name)
    sys.exit(1)
