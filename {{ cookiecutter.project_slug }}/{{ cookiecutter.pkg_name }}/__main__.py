#!/usr/bin/env python
"""
{{ cookiecutter.pkg_name }}.py
"""

import sys
import os

__author__ = "{{ cookiecutter.author }} <{{ cookiecutter.author_email }}>"

def main(args = None):
    """The main application"""
    return 0

if __name__ == "__main__":
    sys.exit(main())
