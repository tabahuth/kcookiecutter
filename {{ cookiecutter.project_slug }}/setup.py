import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "{{ cookiecutter.project_slug }}",
    version = "0.1.0",
    author = "{{ cookiecutter.author }}",
    author_email = "{{ cookiecutter.author_email }}",
    description =  "{{ cookiecutter.project }}",
    license = "BSD",
    url = "http://packages.python.org/{{ cookiecutter.project }}",
    packages = ["{{ cookiecutter.pkg_name }}", "tests"],
    long_description = read("README.rst"),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires = read("requirements.in").splitlines(),
    entry_points={
        'console_scripts': [
            '{{ cookiecutter.project_slug }} = {{ cookiecutter.pkg_name }}.__main__:main'
        ]
    },
)
