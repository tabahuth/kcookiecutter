=============
KCookieCutter
=============

Powered by Cookiecutter_. This is a template for a Python project. To use,
install cookiecutter and run the command::

  cookiecutter https://tabahuth@bitbucket.org/tabahuth/kcookiecutter.git

and fill the requested information.



.. _Cookiecutter: https://cookiecutter.readthedocs.io/
