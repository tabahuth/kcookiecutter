====
TODO
====

* Cache packages for offline use of packages
* pip seems to have an issue when working from inside a virtualenv with a path
  that contains a space. Search for a resolution.
  * In macOS/OS X (and seemingly FreeBSD) *env -S* would be a way forward.
  * Linux can be tested in a Docker environment
